//
//  User.swift
//  Empresas
//
//  Created by Taina Viriato on 11/07/2018.
//  Copyright © 2018 tainavm. All rights reserved.
//

import Foundation

struct User: Mappable {
    
    var id: Int
    var name: String = ""
    var email: String = ""
    
    init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.name = mapper.keyPath("investor_name")
        self.email = mapper.keyPath("email")
        
    }
}
