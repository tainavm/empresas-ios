//
//  Enterprise.swift
//  Empresas
//
//  Created by Taina Viriato on 11/07/2018.
//  Copyright © 2018 tainavm. All rights reserved.
//

import Foundation
import UIKit

struct Empresa: Mappable {

    var id: Int
    var name: String = ""
    var description: String = ""
    var city: String = ""
    var country: String = ""
//    var category: String = ""

    init(mapper: Mapper) {

        self.id = mapper.keyPath("id")
        self.name = mapper.keyPath("enterprise_name")
        self.description = mapper.keyPath("description")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
//        self.category = mapper.keyPath("enterprise_type_name")

    }

}
