//
//  RequestEmpresaAPI.swift
//  Empresas
//
//  Created by Taina Viriato on 12/07/2018.
//  Copyright © 2018 tainavm. All rights reserved.
//

import Foundation

class RequestEmpresaAPI: APIRequest {
    
    static func getEmpresas(successBlock: (([Empresa])->Void)?, failureBlock: ((API.RequestError) -> Void)?) {
        let request = AuthenticationAPI(method: .get, path: "enterprises", parameters: nil, urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            
            if let error = error {
                failureBlock?(error)
            } else if let response = response as? [String: Any], let enterprisesJSON = response["enterprises"] as? [ [String: Any] ] {
                
                var enterprises = [Empresa]()
                
                for dic in enterprisesJSON {
                    let enterprise = Empresa(dictionary: dic)
                    enterprises.append(enterprise)
                }
                
                successBlock?(enterprises)
            }
        }
        
        request.shouldSaveInCache = false
        
        request.makeRequest()
    }
    
}
