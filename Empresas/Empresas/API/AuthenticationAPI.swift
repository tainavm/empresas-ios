//
//  UserAPI.swift
//  iOS Helpers Swift
//
//  Created by Jota Melo on 15/02/17.
//  Copyright © 2017 Jota. All rights reserved.
//

import UIKit

class AuthenticationAPI: APIRequest {
    
    static func loginWith(email: String, password: String, successBlock: ((User)-> Void)?, failureBlock: ((API.RequestError) -> Void)?) {
        
        let request = AuthenticationAPI(method: .post, path: "users/auth/sign_in", parameters: ["email": email, "password": password], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            
            if let error = error {
                failureBlock?(error)
            } else if let response = response as? [String: Any], let investor = response["investor"] as? [String: Any] {
                let user = User(dictionary: investor)
                successBlock?(user)
            }
        }
        
        request.shouldSaveInCache = false
        
        request.makeRequest()
    }

}

