//
//  DetailViewController.swift
//  Empresas
//
//  Created by Taina Viriato on 11/07/2018.
//  Copyright © 2018 tainavm. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
   
    
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var nomeEmpresa: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var infoEmpresa: UITextView!
    
    var empresa: Empresa?

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.nomeEmpresa.text = empresa?.name
        self.infoEmpresa.text = empresa?.description
        
    }

    @IBAction func backBtn(_ sender: Any) {
        performSegue(withIdentifier: "backMain", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
