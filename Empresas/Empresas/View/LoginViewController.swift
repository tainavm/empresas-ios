//
//  LoginViewController.swift
//  Empresas
//
//  Created by Taina Viriato on 10/07/2018.
//  Copyright © 2018 tainavm. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,  UITextFieldDelegate  {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    
    let MSG_ERROR_CAMPOS = "Campos não podem estar vazios"
    let MSG_ERROR = "Erro ao logar. Verifique as informações"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.spinner.isHidden = true
        self.email.delegate = self
        self.password.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Verifica se os campos foram preenchidos
    func isEmailOrPasswordIsEmpty() -> Bool {
        if !self.email.hasText || !self.password.hasText {
            return true
        }
        return false
    }
    
    // Inicia o alerta
    func startAleart(msg: String) {
        let alert = UIAlertController(title: "Erro", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    // Incia o spinner
    func initSpinner(value: Bool) {
        if value {
            spinner.isHidden = true
            spinner.stopAnimating()
        } else {
            spinner.isHidden = false
            spinner.startAnimating()
        }
    }
    
    // Fornece a navegação
    func goToMain() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier: "MainView") as! MainViewController
        self.present(mainViewController, animated:true, completion:nil)
    }
    
    @IBAction func btnSignIn(_ sender: Any) {
        
        let emailTF = email.text!
        let passwordTF = password.text!
        
        if isEmailOrPasswordIsEmpty() {
            startAleart(msg: MSG_ERROR_CAMPOS)
        } else {
            self.initSpinner(value: false)
            // Chamada da API
            AuthenticationAPI.loginWith(email: emailTF, password: passwordTF, successBlock: { (user) in
                self.initSpinner(value: true)
                self.goToMain()
            }) { (error) in
                self.initSpinner(value: false)
                self.startAleart(msg: self.MSG_ERROR)
            }
        }
    }
}


