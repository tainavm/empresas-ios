//
//  MainViewController.swift
//  Empresas
//
//  Created by Taina Viriato on 11/07/2018.
//  Copyright © 2018 tainavm. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    let MSG_ERROR_LOAD = "Erro ao carregar. Tente novamente"
    
    var empresas: [Empresa] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }

    var filtro: [Empresa]? {
        didSet {
            self.tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        
        tableView.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Carrega a lista de empresas
    func requestEmpresas() {
        // Chamada da API
        RequestEmpresaAPI.getEmpresas(successBlock: { (apiEnterprises) in
            self.empresas = apiEnterprises
            
        }, failureBlock: { error in
            self.startAleart(msg: self.MSG_ERROR_LOAD)
        })
    }

    
    // Inicia o alerta
    func startAleart(msg: String) {
        let alert = UIAlertController(title: "Erro", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    // MARK: Setup search
    
     fileprivate func empresa(from indexPath: IndexPath) -> Empresa? {
        if let filtrado = self.filtro, filtrado.count > indexPath.row {
            return filtrado[indexPath.row]
        }
        
        return self.empresas[indexPath.row]
    }
    
     fileprivate func search(term: String?) {
        guard let text = term, !text.isEmpty else {
            self.filtro = nil
            return
        }
        self.filtro = self.empresas.filter({
            $0.name.lowercased().contains(text.lowercased())
        })
    }
    
    
      // MARK: Setup table view
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let filtrado = self.filtro {
            return filtrado.count
        }
        return self.empresas.count
    }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmpresasCell", for: indexPath) as! EmpresasCell
        
        let empresa = self.empresa(from: indexPath)
     
        cell.empresaName.text = empresa?.name
        cell.empresaCountry.text = empresa?.country
//        cell.empresaCategory.text = empresa?.category
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "details"{
            
            if let indexPath = tableView.indexPathForSelectedRow {
                let empresaSelecionada = self.empresa(from: indexPath)
                let destinyViewController = segue.destination as? DetailViewController
                destinyViewController?.empresa = empresaSelecionada
            }
        }
    }

    // MARK: Setup search bar
  
    @IBAction func searchButton(_ sender: Any) {
        self.requestEmpresas()
        self.searchBar.isHidden = false
        self.searchBar.becomeFirstResponder()
        self.tableView.isHidden = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.search(term: self.searchBar.text)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.search(term: self.searchBar.text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.filtro = nil
        self.view.endEditing(true)
        self.searchBar.text = ""
        self.searchBar.showsCancelButton = false
        self.tableView.isHidden = true
        self.searchBar.isHidden = true
        self.searchBar.resignFirstResponder()
    }
    
}

class EmpresasCell: UITableViewCell {
    
    @IBOutlet weak var empresaLogo: UIImageView!
    
    @IBOutlet weak var empresaName: UILabel!
    
    @IBOutlet weak var empresaCategory: UILabel!
    
    @IBOutlet weak var empresaCountry: UILabel!
}


